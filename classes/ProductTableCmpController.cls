public class ProductTableCmpController {

    @AuraEnabled
    public static String getProducts(string stringForSearchJSONfB) {
        return returnData(stringForSearchJSONfB);
    }

    public static String returnData(string stringForSearchJSONfB) {
        string stringForSearch = (string) JSON.deserialize(stringForSearchJSONfB, string.class);

        if (IthinkItsDate(stringForSearch)) {

            Datetime dtFrom = Datetime.newInstance(dt.year(), dt.month(), dt.day(), 0, 0, 0);
            Datetime dtTo = Datetime.newInstance(dt.year(), dt.month(), dt.day(), 23, 59, 59);
            List < ProductTable__c > products = [
                SELECT Name, Amount__c, Price__c, ProductType__c, ReleaseDate__c, Available__c, AddedDate__c
                FROM ProductTable__c
                WHERE AddedDate__c >=: dtFrom AND AddedDate__c <=: dtTo
                //ORDER BY AddedDate__c DESC
            ];
            return JSON.serialize(products);
        } else {
            string searchProductWithCovering = '%' + stringForSearch + '%';
            List < ProductTable__c > products = [
                SELECT Name, Amount__c, Price__c, ProductType__c, ReleaseDate__c, Available__c, AddedDate__c
                FROM ProductTable__c
                WHERE Name LIKE: searchProductWithCovering
                //ORDER BY AddedDate__c DESC
            ];
            return JSON.serialize(products);
        }
    }
    @AuraEnabled
    public static String insertNewProductTable(String newProductJSON, String stringForSearchJSONfB) {
        ProductTable__c product = (ProductTable__c) JSON.deserialize(newProductJSON, ProductTable__c.class);
        try {
            insert product;
        } catch (exception e) {
            throw new AuraHandledException('Hello from BackEnd! ' + e.getMessage());
        }
        return returnData(stringForSearchJSONfB);
    }
    @AuraEnabled
    public static String deleteProductTable(String idProductForDelete, String stringForSearchJSONfB) {
        string idProduct = (string) JSON.deserialize(idProductForDelete, string.class);

        try {
            ProductTable__c pr = new ProductTable__c(Id = idProduct);
            delete pr;
        } catch (exception e) {
            throw new AuraHandledException('Hello from BackEnd! ' + e.getMessage());
        }
        return returnData(stringForSearchJSONfB);
    }
    @AuraEnabled
    public static String updateNewProductTable(String updateProductJSON, String idProductForUpdate, String stringForSearchJSONfB) {
        string idProduct = (string) JSON.deserialize(idProductForUpdate, string.class);
        ProductTable__c product = (ProductTable__c) JSON.deserialize(updateProductJSON, ProductTable__c.class);
        product.Id = idProduct;
        try {
            update product;
        } catch (exception e) {
            throw new AuraHandledException('Hello from BackEnd! ' + e.getMessage());
        }
        return returnData(stringForSearchJSONfB);
    }

    @AuraEnabled
    public static String getPickListValues() {
        Schema.DescribeFieldResult fieldResult = ProductTable__c.ProductType__c.getDescribe();
        List < Schema.PicklistEntry > ple = fieldResult.getPicklistValues();
        List < String > pickListValuesList = new List < String > ();
        for (Schema.PicklistEntry pickListVal: ple) {
            pickListValuesList.add(pickListVal.getLabel());
        }
        return JSON.serialize(pickListValuesList);
    }

    public static Datetime dt {
        get;
        set;
    }
    public static boolean IthinkItsDate(string input) {
        if (input.length() == 10) {
            string[] dateForSearch = input.split('\\.', 3);
            integer dayForSearch = integer.valueOf(dateForSearch[0]);
            integer monthForSearch = integer.valueOf(dateForSearch[1]);
            integer yearForSearch = integer.valueOf(dateForSearch[2]);
            if (dateForSearch.size() == 3 && dayForSearch > 0 && dayForSearch <= 31 && monthForSearch > 0 && monthForSearch <= 12 && yearForSearch >= 1000 && yearForSearch <= 9999) {
                try {
                    dt = Datetime.newInstance(yearForSearch, monthForSearch, dayForSearch);
                    return true;
                } catch (Exception e) {
                    System.debug('Something go wrong with search date: ' + e.getmessage());
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }








}